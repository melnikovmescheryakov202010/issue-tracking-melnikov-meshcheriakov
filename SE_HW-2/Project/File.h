#pragma once
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class FileS
{
	const string path = "Aquariums.txt";
	const string pathOrder = "Order.txt";
	const string copyPath = "Order1.txt";
	unsigned int fileSize;

public:

	struct aquariums
	{
		string id;
		string volume;
		string shape;
		string material;
		double cost;
	};

	void createFile(string _id, string _volume, string _shape, string _material, double _cost)
	{
		ofstream inFile(path);

		inFile << _id << ' ';
		inFile << _volume << ' ';
		inFile << _shape << ' ';
		inFile << _material << ' ';
		inFile << _cost << endl;

		inFile.close();
	}

	void addAquarium(string _id, string _volume, string _shape, string _material, double _cost)
	{
		ofstream inFile(path, ios::app);

		if (!inFile)
		{
			cout << "Файл не открыт. Возможно, необходимо создать файл";
		}
		else
		{
			inFile << _id << ' ';
			inFile << _volume << ' ';
			inFile << _shape << ' ';
			inFile << _material << ' ';
			inFile << _cost << endl;

			inFile.close();
		}
	}

	void printAquariumList()
	{
		ifstream fromFile(path);

		if (!fromFile)
		{
			cout << "Файл не открыт. Возможно, необходимо создать файл" << endl;
		}
		else
		{
			aquariums printedAquarium{};
			unsigned int counter{ 1 };
			fileSize = 0;
			while (true)
			{
				fromFile >> printedAquarium.id;
				fromFile >> printedAquarium.volume;
				fromFile >> printedAquarium.shape;
				fromFile >> printedAquarium.material;
				fromFile >> printedAquarium.cost;

				if (fromFile.eof())
				{
					break;
				}

				cout << counter << ". " << printedAquarium.id << ' ';
				cout << printedAquarium.volume << ' ';
				cout << printedAquarium.shape << ' ';
				cout << printedAquarium.material << ' ';
				cout << printedAquarium.cost << endl;
				counter++;
				fileSize++;
			}

			fromFile.close();
		}
	}

	aquariums findAquarium(unsigned int aquariumNum)
	{
		ifstream fromFile(path);

		if (!fromFile)
		{
			cout << "Файл не открыт. Возможно, необходимо создать файл";
		}
		else
		{
			aquariums foundedAquarium{};
			unsigned int counter{ 1 };
			string buf;
			while (true)
			{
				if (counter == aquariumNum)
				{
					break;
				}
				getline(fromFile, buf);
				counter++;
			}

			fromFile >> foundedAquarium.id;
			fromFile >> foundedAquarium.volume;
			fromFile >> foundedAquarium.shape;
			fromFile >> foundedAquarium.material;
			fromFile >> foundedAquarium.cost;

			fromFile.close();

			return foundedAquarium;
		}
	}

	void deleteAquarium(unsigned int aquariumNum)
	{
		unsigned int counter{ 1 };
		aquariums deletedAquarium;

		deletedAquarium = findAquarium(aquariumNum);

		fstream file(path, ios::in | ios::out);

		if (!file)
		{
			cout << "Файл не открыт. Возможно, необходимо создать файл";
		}
		else
		{
			string* buf;
			buf = new string[fileSize];
			unsigned int i{};

			while ((i + 1) != fileSize)
			{
				string copy;
				file >> copy;
				if (copy == deletedAquarium.id)
				{
					string cache;
					getline(file, cache);
					continue;
				}
				int len = copy.length();
				file.seekg(-len, ios::cur);
				file.seekp(file.tellp(), ios::beg);

				getline(file, buf[i]);
				i++;
			}
			file.close();

			ofstream inFile(path);

			for (int j{}; j < i; j++)
			{
				inFile << buf[j] << endl;
			}

			inFile.close();
			delete[] buf;
		}
	}

	void createOrderFile()
	{
		ofstream inFile(pathOrder);
		inFile.close();
	}

	void addOrder(string _name, string _surname, string _idAquarium)
	{
		ifstream readFile(pathOrder, ios::in);
		ofstream inFile(pathOrder, ios::app);

		int maxId = 0;

		while (true)
		{
			if (readFile.eof())
			{
				break;
			}

			readFile >> maxId;
			string name, surname;
			int idAquarium;
			readFile >> name;
			readFile >> surname;
			readFile >> idAquarium;
		}

		if (!inFile)
		{
			cout << "Файл не открыт. Возможно, необходимо создать файл" << endl;
		}
		else
		{
			inFile << maxId + 1 << ' ';
			inFile << _name << ' ';
			inFile << _surname << ' ';
			inFile << _idAquarium << endl;
		}

		readFile.close();
		inFile.close();
	}

	void searchingOrder(string _surname, string _name)
	{
		ifstream fromFile(pathOrder);

		if (!fromFile)
		{
			cout << "Файл не открыт. Возможно, необходимо создать файл" << endl;
		}
		else
		{
			struct Orders
			{
				int id;
				string name;
				string surname;
				int idAquariums;
			} orderAquarium{};

			while (true)
			{
				fromFile >> orderAquarium.id;
				fromFile >> orderAquarium.name;
				fromFile >> orderAquarium.surname;
				fromFile >> orderAquarium.idAquariums;

				if (fromFile.eof())
				{
					break;
				}

				if ((orderAquarium.name == _name) && (orderAquarium.surname == _surname))
				{
					cout << orderAquarium.id << ' ';
					cout << orderAquarium.name << ' ';
					cout << orderAquarium.surname << ' ';
					cout << orderAquarium.idAquariums << endl;
				}
			}

			fromFile.close();
		}
	}

	void deleteOrder(int id)
	{
		ifstream fromFile(pathOrder);
		ofstream inFile(copyPath);
		if (!fromFile)
		{
			cout << "Файл не открыт. Возможно, необходимо создать файл" << endl;
		}
		else
		{
			struct Orders
			{
				int id;
				string name;
				string surname;
				int idAquariums;
			} orderAquarium{};

			while (true)
			{
				fromFile >> orderAquarium.id;
				fromFile >> orderAquarium.name;
				fromFile >> orderAquarium.surname;
				fromFile >> orderAquarium.idAquariums;

				if (fromFile.eof())
				{
					break;
				}

				if (orderAquarium.id != id)
				{
					inFile << orderAquarium.id << ' ';
					inFile << orderAquarium.name << ' ';
					inFile << orderAquarium.surname << ' ';
					inFile << orderAquarium.idAquariums << endl;
					inFile << orderAquarium.idAquariums << endl;
				}
			}

			inFile.close();
			fromFile.close();

			remove("Order.txt");
			rename("Order1.txt", "Order.txt");
		}
	}
};
