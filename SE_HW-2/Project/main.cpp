#include <iostream>
#include "Aquarium.h"
#include "Selling.h"

using namespace std;

int main()
{
    setlocale(0, "");
    int ch, key;
    cout << "Выберете роль:" << endl;
    cout << "1. Продавец" << endl;
    cout << "2. Покупатель" << endl;
    cin >> key;
    Selling sell(0, "", "", "");
    sell.createFile();
    do
    {
        system("cls");
        if (key == 1)
        {
            cout << "Меню" << endl;
            cout << "1. Создание файла для хранения информации об аквариумах" << endl;
            cout << "2. Закупка аквариумов" << endl;
            cout << "3. Список всех аквариумов" << endl;
            cout << "4. Изменить характеристики аквариума" << endl;
            cout << "5. Выход" << endl;
            cout << "Введите номер действия: ";
            cin >> ch;
            switch (ch)
            {
            case 1:
            {
                FileS computersFile;
                FileS::aquariums firstAquarium;

                system("cls");
                cout << "Введите id первого аквариума: ";
                cin >> firstAquarium.id;
                cout << "Введите объём аквариума: ";
                cin >> firstAquarium.volume;
                cout << "Введите форму аквариума: ";
                cin >> firstAquarium.shape;
                cout << "Введите материал аквариума: ";
                cin >> firstAquarium.material;
                cout << "Введите стоимость: ";
                cin >> firstAquarium.cost;

                computersFile.createFile(firstAquarium.id, firstAquarium.volume, firstAquarium.shape, firstAquarium.material, firstAquarium.cost);
            }
            break;
            case 2:
            {
                FileS::aquariums newAquarium;

                system("cls");
                cout << "Введите id нового аквариума: ";
                cin >> newAquarium.id;
                cout << "Введите объём аквариума: ";
                cin >> newAquarium.volume;
                cout << "Введите форму аквариума: ";
                cin >> newAquarium.shape;
                cout << "Введите материал аквариума: ";
                cin >> newAquarium.material;
                cout << "Введите стоимость: ";
                cin >> newAquarium.cost;

                Aquarium boughtAquarium(newAquarium.id, newAquarium.volume, newAquarium.shape, newAquarium.material, newAquarium.cost);

                boughtAquarium.buyAquarium();
            }
            break;
            case 3:
            {
                Aquarium aquariumsList;
                system("cls");
                aquariumsList.printAquariums();
                system("pause");
            }
            break;
            case 4:
            {
                system("cls");
                Aquarium changeAquarium;
                changeAquarium.changeSpecifications();
            }
            break;
            case 5:
            {
                exit(0);
            }
            break;
            }
        }
        else
        {
            cout << "Меню" << endl;
            cout << "1. Заказать аквариум" << endl;
            cout << "2. Оплатить аквариум" << endl;
            cout << "3. Вернуть аквариум" << endl;
            cout << "4. Выход" << endl;
            cout << "Введите номер действия: ";
            cin >> ch;

            switch (ch)
            {
            case 1:
            {
                system("cls");
                sell.orderAquarium();
            }
            break;
            case 2:
            {
                system("cls");
                sell.payOrder();
            }
            break;
            case 3:
            {
                system("cls");
                sell.returnAquarium();
            }
            break;
            case 4:
            {
                exit(0);
            }
            break;
            }
        }
    } while (true);

    return 0;
}