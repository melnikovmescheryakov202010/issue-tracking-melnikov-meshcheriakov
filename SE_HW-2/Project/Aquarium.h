#pragma once
#include <string>
#include <iostream>
#include "File.h"

using namespace std;

class Aquarium
{
public:

	string id;
	string volume;
	string shape;
	string material;
	double cost;

	Aquarium(string _id, string _volume, string _shape, string _material, double _cost)
	{
		id = _id;
		volume = _volume;
		shape = _shape;
		material = _material;
		cost = _cost;
	}

	Aquarium() {}

	void buyAquarium()
	{
		FileS aquariumFile;
		aquariumFile.addAquarium(id, volume, shape, material, cost);
	}

	void printAquariums()
	{
	
	FileS aquariumFile;
	aquariumFile.printAquariumList();
	
	}

	void changeSpecifications()
	{
	
		setlocale(0, "");
		FileS aquariumFile;
		aquariumFile.printAquariumList();
		cout << endl;

		unsigned int aquariumNumber, counter{ 1 };
		cout << "Введите номер аквариума в списке для изменения характеристик: ";
		cin >> aquariumNumber;
		system("cls");

		FileS::aquariums changedAquarium;
		changedAquarium = aquariumFile.findAquarium(aquariumNumber);

		int ch;
		bool check{};
		do
		{
			system("cls");
			cout << "1. Изменить id аквариума." << endl;
			cout << "2. Изменить объём аквариума." << endl;
			cout << "3. Изменить форму аквариума." << endl;
			cout << "4. Изменить материал аквариума." << endl;
			cout << "5. Изменить стоимость аквариума." << endl;
			cout << "6. Выход." << endl;
			cout << "Введите номер действия: ";
			cin >> ch;
			switch (ch)
			{
			case 1:
			{
				cout << "Введите новый id: ";
				cin >> changedAquarium.id;
			}
			break;
			case 2:
			{
				cout << "Введите новый объём: ";
				cin >> changedAquarium.volume;
			}
			break;
			case 3:
			{
				cout << "Введите новую форму: ";
				cin >> changedAquarium.shape;
			}
			break;
			case 4:
			{
				cout << "Введите новый материал: ";
				cin >> changedAquarium.material;
			}
			break;
			case 5:
			{
				cout << "Введите новую стоимость: ";
				cin >> changedAquarium.cost;
			}
			break;
			case 6:
			{
				check = 1;
			}
			}
		} while (check != 1);

		aquariumFile.deleteAquarium(aquariumNumber);
		aquariumFile.addAquarium(changedAquarium.id, changedAquarium.volume, changedAquarium.shape, changedAquarium.material, changedAquarium.cost);
	
	}
};
