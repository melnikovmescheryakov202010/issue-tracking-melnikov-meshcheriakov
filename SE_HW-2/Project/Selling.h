#pragma once
#include "File.h"

class Selling
{
public:
	int id;
	string surname, name, idAquarium;
	FileS file;

	Selling(int _id, string _name, string _surname, string _idAquarium)
	{
		id = _id;
		name = _name;
		surname = _surname;
		idAquarium = _idAquarium;
	}

	void createFile()
	{
	file.createOrderFile();
	}

	void orderAquarium()
	{
	file.printAquariumList();
	cout << "Введите id товара, который хотите заказать: ";
	cin >> idAquarium;
	system("pause");
	}

	void payOrder()
	{
		file.printAquariumList();
		cout << "Ваш товар с идентификатором " << idAquarium << ". Вы уверены что хотите его приобрести? 1 - Да; 2 - Нет" << endl;
		int key = 0;
		cin >> key;
		if (key == 1)
		{
			cout << "Введите свою фамилию: ";
			cin >> surname;
			cout << "Введите своё имя: ";
			cin >> name;
			file.addOrder(name, surname, idAquarium);
			cout << "Ваш товар заказан. Оплата при получении товара." << endl;
		}
		else
		{
			cout << "Ваш товар не оплачен. Вы можете выбрать другой аквариум." << endl;
		}
		system("pause");
	}

	void returnAquarium()
	{
		cout << "Список всех аквариумов:" << endl;
		file.printAquariumList();
		cout << "Введите свою фамилию: ";
		cin >> surname;
		cout << "Введите своё имя: ";
		cin >> name;
		file.searchingOrder(surname, name);
		cout << "Введите id заказа: ";
		cin >> id;
		file.deleteOrder(id);
		cout << "Ваш товар возвращён.";
		system("pause");
	}
};